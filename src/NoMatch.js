import React from 'react';

export const NoMatch = () => {
    return(
        <h2>No page found !</h2>
    );
}