export const addCourse = (course) => {
    return {
        type: 'ADD_COURSE',
        course: course
    }
}

export const editCourse = (course) => {
    return {
        type: 'EDIT_COURSE',
        course: course
    }
}

export const onRowSelect = (course) => {
    return {
        type: 'ON_ROW_SELECT',
        course: course
    }
}

export const deleteCourse = (courseId) => {
    return {
        type: 'DELETE_COURSE',
        courseId: courseId
    }
}