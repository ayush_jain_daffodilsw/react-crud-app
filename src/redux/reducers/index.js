import { combineReducers } from 'redux';
import CourseReducer from './Course';

const allReducers = combineReducers({
    course: CourseReducer,
});

export default allReducers;