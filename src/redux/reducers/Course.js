const initialState = {
    courseList: [
        {
            id: 1,
            title: 'Building Applications in React and Flux',
            link: 'http://www.pluralsight.com/courses/react-flux-building-applications',
            author: 'Cory House',
            category: 'JavaScript',
            length: '5:08'
        },
        {
            id: 2,
            title: 'Clean Code: Writing Code for Humans',
            link: 'http://www.pluralsight.com/courses/writing-clean-code-humans',
            author: 'Cory House',
            category: 'Software Practices',
            length: '3:10'
        },
        {
            id: 3,
            title: 'Architecting Applications for the Real World',
            link: 'http://www.pluralsight.com/courses/architecting-applications-dotnet',
            author: 'Cory House',
            category: 'Software Architecture',
            length: '2:52'
        },
        {
            id: 4,
            title: 'Becoming an Outlier: Reprogramming the Developer Mind',
            link: 'http://www.pluralsight.com/courses/career-reboot-for-developer-mind',
            author: 'Cory House',
            category: 'Career',
            length: '2:30'
        },
        {
            id: 5,
            title: 'Web Component Fundamentals',
            link: 'http://www.pluralsight.com/courses/web-components-shadow-dom',
            author: 'Cory House',
            category: 'HTML5',
            length: '5:10'
        },
    ],

    selectedCourseRowData: {
        id: '',
        title: '',
        author: '',
        category: '',
        length: '',
        link: ''
    }
}

const CourseReducer = (state=initialState, action) => {
    switch(action.type){
            
        case 'ADD_COURSE':
            let id = state.courseList.length+1;
            action.course.id = id;
            action.course.link='https://www.google.com';
            return {
                ...state,
                courseList: [
                    ...state.courseList,
                    Object.assign({}, action.course)
                ],
                selectedCourseRowData: {
                    ...state.selectedCourseRowData
                }
            }
            
        case 'EDIT_COURSE':
            return {
                ...state,
                courseList: state.courseList.map((course) => {
                    let temp = Object.assign({}, course);
                    if(temp.id === action.id){
                        temp.title = action.title;
                        temp.author = action.author;
                        temp.category = action.category;
                        temp.length = action.length;
                    }
                    return temp;
                }),
                selectedCourseRowData: {id: '', title: '', author: '', category: '', length: '', link: ''}
            }

        case 'DELETE_COURSE':
            return {
                ...state,
                courseList: state.courseList.filter((course) => {
                    return course.id !== action.courseId;
                }),
                selectedCourseRowData: {id: '', title: '', author: '', category: '', length: '', link: ''}
            }

        case 'ON_ROW_SELECT':
            return {
                ...state,
                courseList: [
                    ...state.courseList
                ],
                selectedCourseRowData: action.course
            }

        default:
            return state;
            
    }
}

export default CourseReducer;