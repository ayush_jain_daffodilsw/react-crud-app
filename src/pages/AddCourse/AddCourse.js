import React from 'react';
import {Form, FormGroup, Label, Input, Button} from 'reactstrap';
import { Link, Redirect } from "react-router-dom";
import { addCourse } from '../../redux/actions/action';
import { connect } from 'react-redux';

import './AddCourse.css';

class AddCourse extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            newCourseData: {
                title: '',
                author: '',
                length: '',
                category: ''
            },
            operationStatus: false,
            titleError: '',
            authorError: '',
            categoryError: '',
            lengthError: ''
        }
    }

    validate = () => {
        let titleError = '';
        let authorError = '';
        let categoryError = '';
        let lengthError = '';
        if(!this.state.newCourseData.title){
            titleError = 'Required';
        }
        if(!this.state.newCourseData.author){
            authorError = 'Required';
        }
        if(!this.state.newCourseData.category){
            categoryError = 'Required';
        }
        if(!this.state.newCourseData.length){
            lengthError = 'Required';
        }
        if(titleError || authorError || categoryError || lengthError){
            this.setState({titleError, authorError, categoryError, lengthError});
            return false;
        }
        return true;
    }

    //Function: Add Course Form Submit
    handleSubmit = (e) => {
        e.preventDefault();
        const isValid = this.validate();
        if(isValid){
            this.props.addCourse(this.state.newCourseData);
            this.setState({
                operationStatus: true
            })
        }
    }

    //Function: to clear form data
    resetFormData = () => {
        this.setState({
            newCourseData: {title: '', author: '', length: '', category: ''}
        })
    }

    render(){
        const operationStatus = this.state.operationStatus;
        if (operationStatus === true) {
            return <Redirect to="/" />
        }
        return(
            <div className="m-3 container">
                <h2>Add Course</h2>
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <Label for="title">Title</Label>
                        <Input type="text" name="title" id="title" value={this.state.newCourseData.title} placeholder="Title of the course" onChange={(e) => {
                            let {newCourseData} = this.state;
                            newCourseData.title = e.target.value;

                            this.setState({
                                newCourseData: newCourseData
                            })
                        }} />
                        <div className='error-message'>{this.state.titleError}</div>
                    </FormGroup>
                    <FormGroup>
                        <Label for="author">Author</Label>
                        <Input type="select" name="author" id="author" value={this.state.newCourseData.author} onChange={(e) => {
                            let {newCourseData} = this.state;
                            newCourseData.author = e.target.value;

                            this.setState({
                                newCourseData: newCourseData
                            })
                        }}>
                            <option></option>
                            <option>Cory House</option>
                            <option>Scott Allen</option>
                            <option>Dan Wahlin</option>
                        </Input>
                        <div className='error-message'>{this.state.authorError}</div>
                    </FormGroup>
                    <FormGroup>
                        <Label for="category">Category</Label>
                        <Input type="text" name="category" id="category" value={this.state.newCourseData.category} placeholder="Category of the course" onChange={(e) => {
                            let {newCourseData} = this.state;
                            newCourseData.category = e.target.value;

                            this.setState({
                                newCourseData: newCourseData
                            })
                        }} />
                        <div className='error-message'>{this.state.categoryError}</div>
                    </FormGroup>
                    <FormGroup>
                        <Label for="length">Length</Label>
                        <Input type="text" name="length" id="length" value={this.state.newCourseData.length} placeholder="Length of course in minutes or hours" onChange={(e) => {
                            let {newCourseData} = this.state;
                            newCourseData.length = e.target.value;

                            this.setState({
                                newCourseData: newCourseData
                            })
                        }} />
                        <div className='error-message'>{this.state.lengthError}</div>
                    </FormGroup>

                    <Button color="primary" className="mr-4"><i className="fa fa-paper-plane-o" aria-hidden="true"/>   Submit</Button>
                    <Button color="secondary" className="mr-4" onClick={this.resetFormData}>Clear Value</Button>
                    <Link to="/"><Button color="secondary">Cancel</Button></Link>
                </Form>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addCourse: (course) => dispatch(addCourse(course))
    }
}

export default connect(null, mapDispatchToProps)(AddCourse);