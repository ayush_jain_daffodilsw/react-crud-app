import React from 'react';
import {Form, FormGroup, Label, Input, Button} from 'reactstrap';
import { Link, Redirect } from "react-router-dom";
import { editCourse } from '../../redux/actions/action';

import { connect } from 'react-redux';

class EditCourse extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editCourseData: {
                title: '',
                author: '',
                length: '',
                category: '',
                link: '',
                id: ''
            },
            operationStatus: false,
            titleError: '',
            authorError: '',
            categoryError: '',
            lengthError: ''
        }
    }

    componentWillMount(){
        this.setState({
            editCourseData: this.props.formData
        });
    }

    validate = () => {
        let titleError = '';
        let authorError = '';
        let categoryError = '';
        let lengthError = '';
        if(!this.state.editCourseData.title){
            titleError = 'Required';
        }
        if(!this.state.editCourseData.author){
            authorError = 'Required';
        }
        if(!this.state.editCourseData.category){
            categoryError = 'Required';
        }
        if(!this.state.editCourseData.length){
            lengthError = 'Required';
        }
        if(titleError || authorError || categoryError || lengthError){
            this.setState({titleError, authorError, categoryError, lengthError});
            return false;
        }
        return true;
    }
    
    //Function: Edit Course Form Submit
    handleSubmit = (e) => {
        e.preventDefault();
        const isValid = this.validate();
        if(isValid){
            this.props.editCourse(this.state.editCourseData);
            this.setState({
                operationStatus: true
            })
        }
    }

    render(){
        const operationStatus = this.state.operationStatus;
        if (operationStatus === true) {
            return <Redirect to="/" />
        }

        return(
            <div className="m-3 container">
                <h2>Edit Course</h2>
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <Label for="title">Title</Label>
                        <Input type="text" name="title" id="title" value={this.state.editCourseData.title} placeholder="Title of the course" onChange={(e) => {
                            let {editCourseData} = this.state;
                            editCourseData.title = e.target.value;

                            this.setState({
                                editCourseData: editCourseData
                            })
                        }} />
                        <div className='error-message'>{this.state.titleError}</div>
                    </FormGroup>
                    <FormGroup>
                        <Label for="author">Author</Label>
                        <Input type="select" name="author" id="author" value={this.state.editCourseData.author} onChange={(e) => {
                            let {editCourseData} = this.state;
                            editCourseData.author = e.target.value;

                            this.setState({
                                editCourseData: editCourseData
                            })
                        }} >
                            <option>Select author</option>
                            <option>Cory House</option>
                            <option>Scott Allen</option>
                            <option>Dan Wahlin</option>
                        </Input>
                        <div className='error-message'>{this.state.authorError}</div>
                    </FormGroup>
                    <FormGroup>
                        <Label for="category">Category</Label>
                        <Input type="text" name="category" id="category" value={this.state.editCourseData.category} placeholder="Category of the course" onChange={(e) => {
                            let {editCourseData} = this.state;
                            editCourseData.category = e.target.value;

                            this.setState({
                                editCourseData: editCourseData
                            })
                        }} />
                        <div className='error-message'>{this.state.categoryError}</div>
                    </FormGroup>
                    <FormGroup>
                        <Label for="length">Length</Label>
                        <Input type="text" name="length" id="length" value={this.state.editCourseData.length} placeholder="Length of course in minutes or hours" onChange={(e) => {
                            let {editCourseData} = this.state;
                            editCourseData.length = e.target.value;

                            this.setState({
                                editCourseData: editCourseData
                            })
                        }} />
                        <div className='error-message'>{this.state.lengthError}</div>
                    </FormGroup>

                    <Button color="primary" className="mr-4"><i className="fa fa-paper-plane-o" aria-hidden="true"/>   Submit</Button>
                    <Link to="/"><Button color="secondary">Cancel</Button></Link>
                </Form>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        formData: state.course.selectedCourseRowData
    }
}

const mapDispatchToProps = dispatch => {
    return {
        editCourse: (course) => dispatch(editCourse(course))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditCourse);