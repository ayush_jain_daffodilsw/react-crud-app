import React from 'react';
import { Link } from "react-router-dom";
import { Button, Table, ButtonGroup } from 'reactstrap';
import { onRowSelect, deleteCourse } from '../../redux/actions/action';
import { connect } from 'react-redux';
import './Home.css';

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedRow: -1
        }
    }
    
    //Function : update selected row data in store
    handleOnClick = selectedRow => e => {
        if (selectedRow !== -1) {
            this.setState({ selectedRow: selectedRow.id  });
        }
        this.props.onRowSelect(selectedRow);
    };

    // Function : To delete a record
    deleteCourse = () => {
        this.props.deleteCourse(this.state.selectedRow);
        this.setState({ selectedRow: -1  });
    }

    render() {
        const courseListItems = this.props.courseList.map((course) => 
            <tr key={course.id} onClick={this.handleOnClick(course)} className={this.state.selectedRow === course.id ? "selected" : "" }>
                <td><a href={course.link} target="_blank" rel="noopener noreferrer">{course.title}</a></td>
                <td>{course.length}</td>
                <td>{course.category}</td>
                <td>{course.author}</td>
            </tr>
        );

        return(
            <div className="m-3">
                <h1>Courses</h1>
                <ButtonGroup>
                    <Link to="/add-course">
                        <Button className="mr-3" color="primary">
                            <i className="fa fa-plus" aria-hidden="true"/>  New
                        </Button>
                    </Link>
                    <Link to="/edit-course">
                        <Button className="mr-3" color="warning" disabled={this.state.selectedRow !== -1 ? false : true }>
                            <i className="fa fa-pencil" aria-hidden="true"/>  Edit
                        </Button>
                    </Link>
                    <Button color="danger" disabled={this.state.selectedRow !== -1 ? false : true } onClick={this.deleteCourse}>
                        <i className="fa fa-trash-o" aria-hidden="true"/>  Delete
                    </Button>
                </ButtonGroup>

                <Table striped className="my-3">
                    <thead>
                        <tr>
                        <th>Title</th>
                        <th>Length</th>
                        <th>Category</th>
                        <th>Author</th>
                        </tr>
                    </thead>
                    <tbody>
                        {courseListItems}
                    </tbody>
                </Table>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        courseList: state.course.courseList
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onRowSelect: (course) => dispatch(onRowSelect(course)),
        deleteCourse: (courseId) => dispatch(deleteCourse(courseId))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);