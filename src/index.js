import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
//import App from './App';
import * as serviceWorker from './serviceWorker';
import Home from './pages/Home';
import AddCourse from './pages/AddCourse';
import EditCourse from './pages/EditCourse';
import {NoMatch} from './NoMatch';

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import{ createStore } from 'redux';
import { Provider } from 'react-redux';
import allReducers from './redux/reducers';

const store = createStore(allReducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()    
)

const routing = (
    <Router>
        <div>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/add-course" component={AddCourse} />
                <Route path="/edit-course" component={EditCourse} />
                <Route path="*" component={NoMatch}/>
            </Switch>     
        </div>
    </Router>
)

ReactDOM.render(<Provider store={store}>{routing}</Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
